//import faker from "faker";
//import { normalize, schema } from "normalizr";
//const mongoose = require("mongoose");
//const app = require("express")();
//const httpServer = require("http").createServer(app);
import { Bienvenida, HolaDeNuevo } from "../pages/pages";
import express from "express";
import { fork } from "child_process";
const randomFork = fork("random.js");

const return400or200 = (res) =>
	res === undefined || res === null ? 400 : 200;

export const root = async (
	req: express.Request,
	reply: express.Response,
) => {
	reply.sendStatus(return400or200(req));
	reply.sendFile(__dirname + "/index.html");
	reply.send(req);
};
export const login = async (
	req: express.Request,
	reply: express.Response,
) => {
	reply.sendStatus(return400or200(req));
	reply.sendFile(__dirname + "/login.html");
	reply.send(req);
};
export const info = async (
	req: express.Request,
	reply: express.Response,
) => {
	reply.sendStatus(return400or200(req));
	reply.send({
		args: process.argv,
		working_dir: process.cwd(),
		version: process.version,
		plataform: process.platform,
		memoryUsage: process.memoryUsage(),
		PID: process.ppid,
	});
	reply.send(req);
};

export const random = async (
	{ params: { cant } }: express.Request,
	reply: express.Response,
) => {
	const cantN = Number(cant);
	cantN === undefined
		? randomFork.send(100000000)
		: randomFork.send(cantN);
	randomFork.on("message", (msg) => {
		reply.send(msg).sendStatus(200);
	});
};

export const postLogin = async (
	req: express.Request,
	reply: express.Response,
) => {
	reply.send(Bienvenida("Usuario N# 1000"));
	/*
	req.session.cookie.expires = new Date(Date.now() + 60000); // La sesion expira al minuto
	req.session.user = nombreUsuario;
	const nombreUsuario =
		req.session.user ?? req.params.user;
	if (nombreUsuario !== undefined) {
		reply.send(HolaDeNuevo(nombreUsuario)); //referencia en ./pages/pages.ts
		return;
	}
	req.session.user = nombreUsuario;
	reply.send(Bienvenida(nombreUsuario)); //referencia en ./pages/pages.ts
	reply.send(req);
 */
};

export const logout = async (
	req: express.Request,
	reply: express.Response,
) =>
	req.session.destroy((err) => {
		!err
			? reply.send("Logout")
			: reply.send({ status: "ERROR", body: err }),
			reply.send(req);
	});
