/* eslint-disable @typescript-eslint/no-var-requires */

import express from "express";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import session from "express-session";
import {
	PORT,
	logPath,
	FACEBOOK_CLIENT_ID,
	FACEBOOK_CLIENT_SECRET,
} from "./process";
import pino from "pino";
import compression from "compression";
import minimist from "minimist";
const logger = pino(),
	faker = require("faker"),
	childProcess = require("child_process"),
	stream = require("stream"),
	logThrough = new stream.PassThrough(),
	log = pino({ name: "project" }, logThrough),
	mongoose = require("mongoose"),
	app = express(),
	client = require("twilio")(accountSid, authToken),
	accountSid = TWILIO_ACCOUNT_SID,
	authToken = TWILIO_AUTH_TOKEN,
	httpServer = require("http").createServer(app),
	nodemailer = require("nodemailer"),
	io = require("socket.io")(httpServer),
	localPort = minimist.port ?? PORT;

app.use(compression());

const child = childProcess.spawn(
	process.execPath,
	[
		require.resolve("pino-tee"),
		"warn",
		`${logPath}/warn.log`,
		"error",
		`${logPath}/error.log`,
		"fatal",
		`${logPath}/fatal.log`,
	],
	process.env,
	process.cwd,
);
logThrough.pipe(child.stdin);

/* ----------------------------------------------------- */
import redis from "redis";
const client = redis.createClient();
const RedisStore = require("connect-redis")(session);
/* ----------------------------------------------------- */
const passport = require("passport");
import { Socket } from "socket.io";
const Strategy = require("passport-facebook").Strategy;

mongoose.connect( process.env.MONGO_URL, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

passport.use(
	new Strategy(
		{
			clientID: FACEBOOK_CLIENT_ID() ?? process.env.FACEBOOK_CLIENT_ID,
			clientSecret: FACEBOOK_CLIENT_SECRET() ?? process.env.FACEBOOK_CLIENT_SECRET,
			callbackURL: "/return",
			profileFields: ["id", "displayName", "photos", "email"],
		},
		function (
			accessToken: unknown,
			refreshToken: unknown,
			profile: unknown,
			cb,
		) {
			return cb(null, profile);
		},
	),
);
app.use(passport.initialize());
app.use(passport.session());
app.use(cookieParser());
app.use(
	session({
		/* ----------------------------------------------------- */
		store: new RedisStore({
			host: "localhost",
			port: 6379,
			client: client,
			ttl: 300,
		}),
		/* ----------------------------------------------------- */
		secret: "secret",
		resave: false,
		saveUninitialized: false,
	}),
);

const { connection: DB } = mongoose;
DB.on(
	"error",
	logger.error("Error on DB connection") &&
		log.error("Error on DB connection"),
);
DB.once("open", function () {
	logger.info("DB opened");
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

import {
	ProductsCRUD,
	MessagesCRUD,
} from "./persistency/DataObjectModelLogic";
import {
	root,
	login,
	info,
	random,
	postLogin,
	logout,
} from "./routes/routes";
app.get("/", (req, reply) => root(req, reply));
app.get("/login", (req, reply) => login(req, reply));
app.get("/info", (req, reply) => info(req, reply));
app.get("/random:cant?", (req, reply) => random(req, reply));
app.post("/login:id", (req, reply) => postLogin(req, reply));

app.set("views", __dirname + "../views");
app.set("view engine", "ejs");
// LOGIN Logout
app.get("/logout", (req, reply) => logout(req, reply));
app.get("/login", (req, res) => {
	res.render("login");
});
app.get("/login/facebook", passport.authenticate("facebook"));

app.get(
	"/return",
	passport.authenticate("facebook", {
		failureRedirect: "/login",
	}),
	(req, res) => {
		res.redirect("/");
		res.send(req);
	},
);

app.get(
	"/profile",
	require("connect-ensure-login").ensureLoggedIn(),
	function (req, res) {
		res.render("profile", { user: req.user });
	},
);

app.post(
	"/register",
	async (req: express.Request, reply: express.Response) => {
		const transporter = nodemailer.createTransport({
			host: process.env.EMAI_LHOST,
			port: 465,
			secure: true,
			auth: {
				user: process.env.EMAIL_ACCOUNT_USER,
				pass: process.env.EMAIL_ACCOUNT_PASSWORD,
			},
		});
		const message = {
			from: String(process.env.EMAIL_ACCOUNT_USER),
			to: String(req.body?.email),
			subject: "Test Subject",
			text: "Test Text",
			html: "<h1>Cool</h1>",
		};
		transporter.sendMail(message);
		client.messages
			.create({
				from: process.env.TWILIONUMBER,
				body: "TEST",
				to: req.body?.number,
			})
			.then((message) => console.log(message.sid));

		// db.records.push(body);
		reply.sendStatus(200);
		reply.send(req);
	},
);

app.get("/index.js", async (res, reply: express.Response) => {
	res === undefined && reply.sendStatus(400);
	reply.sendFile(__dirname + "/index.js");
});

/* GRAPHQL */
import { graphqlHTTPS } from "express-graphql";
import { buildSchema } from "graphql";
const schema = buildSchema(`
type Query {
producto_all: String
producto_id(id: String!): Producto
}
type Producto {
id: String
precio: Int
stock: Int
description: String
}
`);
const root = {
	producto_all: ProductsCRUD.GetAll(),
	producto_id: (id) => ProductsCRUD.Read(id),
};
app.use(
	"/graphql",
	graphqlHTTPS({
		schema: schema,
		rootValue: root,
		graphiql: true,
	}),
);
app.get("/products", async (res, reply: express.Response) => {
	res === undefined && reply.sendStatus(400);
	reply.type("application/json");
	reply.status(200).send(ProductsCRUD.GetAll());
});

app.get("/cdm", async (res, reply: express.Response) => {
	res === undefined && reply.sendStatus(400);
	reply.type("application/json");
	reply.status(200).send(MessagesCRUD.GetAll());
});

app.post("/products", async ({ body }, reply) => {
	if (body === undefined || body === null) {
		reply.send(400);
	}
	reply.status(ProductsCRUD.Create(body));
});

app.post("/cdm", async ({ body }, reply: express.Response) => {
	if (body === undefined || body === null) {
		reply.send(400);
	}
	reply.status(MessagesCRUD.Create(body));
});

app.delete(
	"/products/:id",
	async ({ params: { id } }, reply: express.Response) => {
		const nID = String(id);
		reply.status(ProductsCRUD.Delete(nID));
	},
);

app.get(
	"/products/:id",
	async ({ params: { id } }, reply: express.Response) => {
		const nID = String(id);
		reply.send(ProductsCRUD.Read(nID));
	},
);

const {
	commerce: {
		productName: faker_PN,
		price: faker_P,
		color: faker_C,
	},
} = faker;
app.get(
	"/products/vista_test/:cant",
	async ({ params: { cant } }, reply: express.Response) => {
		const cantNumber: number = Number(cant) || 0;
		if (cantNumber === 0) {
			reply.sendStatus(200).send("No hay productos");
			logger.warn("No hay productos");
		}
		const productos: Array<{
			name: string;
			price: string;
			color: string;
		}> = [];
		for (let i = 0; i < cantNumber; ++i) {
			productos.push({
				name: faker_PN(),
				price: faker_P(),
				color: faker_C(),
			});
		}
		reply.sendStatus(200).send(productos);
	},
);

io.on("connection", (socket: Socket) => {
	console.log("connection");
	socket.on("product", (product) =>
		io.emit("product", product),
	);
	socket.on("message", (messageText) =>
		io.emit("message", messageText),
	);
});

httpServer.listen(localPort()) &&
	logger.info(`Listening to ${localPort()}`);
