import io from "socket.io-client";
const URL = "http://localhost:3000";
const socket = io(URL, { autoConnect: false });
const getEl = (a: string) => document.getElementById(a);
socket.on("product", (text: string) => {
	const producto = document.createElement("li");
	producto.className = "list-group-item";
	producto.innerHTML =
		`Nombre del Producto: ${text[0]}` +
		"<br>" +
		`
Precio:  ${text[1]} ` +
		"<br>" +
		`<img src="${text[2]}" alt="Imagen del Producto"/>`;
	getEl("lista-productos").appendChild(producto);
});

socket.on("message", (messageText: string) => {
	const mensaje = document.createElement("li");
	mensaje.className = "list-group-item";
	mensaje.innerHTML = `<strong>[${
		messageText[0]
	}]</strong><div>[${Date()}]</div><em>[${
		messageText[1]
	}]</em>`;
	getEl("centro-mensajes").appendChild(mensaje);
});

getEl("this-button").onclick = () => {
	const text = [
		getEl("this-input"),
		getEl("this-input2"),
		getEl("this-input3"),
	];
	socket.emit("product", text);
};
getEl("mensaje-button").onclick = () => {
	const messageText = [
		getEl("correo-input"),
		getEl("mensaje-input"),
	];
	socket.emit("message", messageText);
};
// getEl("mensaje-input").onkeypress = () => {};
// getEl("mensaje-input").onkeyup = () => {};
