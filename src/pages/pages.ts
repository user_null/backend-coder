const Head = `
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LOGIN</title>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/milligram/1.4.1/milligram.css"/>
	</head>
  `;
export const Bienvenida = (nombreUsuario: string): string =>
	`${Head}
<body>
<h1> Bienvenido ${nombreUsuario}</h1>
<a  class="button" href="https://localhost:3000/Logout">Logout</a>
</body>
`;
export const HolaDeNuevo = (nombreUsuario: string): string =>
	`${Head}
<body>
<h1> Hola de nuevo ${nombreUsuario}!</h1>
<a  class="button" href="https://localhost:3000/Logout">Logout</a>
</body>
`;
