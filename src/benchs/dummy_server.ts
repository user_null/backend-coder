import express from "express";
const app = express();
app.get("/random/:cant", (req, res) => {
	const {
		params: { cant },
	} = req;
	const cantN = Number(cant);
	const response = [];
	for (let i = 0; i < cantN; i++) {
		response.push(Math.random());
	}
	console.log(`Radom of ${cantN}`);
	res.send(response);
});
app.listen(8080);
