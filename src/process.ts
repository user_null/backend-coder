import memoize from "fast-memoize";
// [NEW] PORT is taked from the command-line, the RegExp detects 8080, 80, or any number < 65535 (See wikipedia https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers#Dynamic,_private_or_ephemeral_ports)
// If that fails, use the PORT in the .env file
// If that fails, use 8080
const { argv, env } = process;
const cwd = process.cwd();
export const logPath = `${cwd}/log`;
export const PORT = memoize(
	() =>
		argv.find(
			(element) =>
				/\d+/.test(element) &&
				Number(element) < 65535 &&
				element,
		) ??
		env[PORT] ??
		8080,
);

const findProp = (arr: Array<string>) => (prop: string) => (
	offset: number,
) => {
	for (let i = 0; ; ) {
		if (arr[i] == prop) return arr[i + offset];
		i++;
	}
};
const findPropFromArgv = findProp(argv);

export const FACEBOOK_CLIENT_ID = memoize(
	() =>
		findPropFromArgv("--facebook-id")(1) ??
		env["FACEBOOK_CLIENT_ID"],
);
export const FACEBOOK_CLIENT_SECRET = memoize(
	() =>
		findPropFromArgv("--facebook-secret")(1) ??
		env["FACEBOOK_CLIENT_SECRET"],
);
