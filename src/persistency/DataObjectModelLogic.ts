import { v4 as uuidv4 } from "uuid";
type Generic = { string?: unknown };
type id = string
const MapProducts = new Map();
const MapMessages = new Map();


const create_ = (MAP: Map<string, unknown>) => (
	OBJ: Generic,
): id => {
	const uuid = uuidv4();
	return MAP.set(uuid, OBJ) ? uuid : "";
};

const read_ = (MAP: Map<string, unknown>) => (
	ID: string,
): Generic | number => MAP.has(ID) ? MAP.get(ID) : 404;

const update_ = (MAP: Map<string, unknown>) => (
	OBJ: Generic,
) => (ID: string): number =>
	MAP.has(ID) ? MAP.set(ID, OBJ) && 200 : 404;

const delete_ = (MAP: Map<string, unknown>) => (
	ID: string,
): number => MAP.has(ID) ? MAP.delete(ID) && 200 : 404;


const ProductsCRUDMemory = {
	Create: async (x):Generic => create_(MapProducts)(x),
	Read: async (x): Generic | number => read_(MapProducts)(x),
	Update: async (x, ID): number => update_(MapProducts)(x)(ID),
	Delete: async (x): number => delete_(MapProducts)(x),
	GetAll: async (): Generic => Object.fromEntries(MapProducts),
};

export const MessagesCRUD = {
	Create: async (x): number => create_(MapMessages)(x),
	Read: async (x): Generic | number => read_(MapMessages)(x),
	Update: async (x, ID): number => update_(MapMessages)(x)(ID),
	Delete: async (x): number => delete_(MapMessages)(x),
	GetAll: async (): Generic => Object.fromEntries(MapMessages),
};

// File Logic
const filePath = process.env.FILE_PATH;
import { appendFile } from "fs";
const ProductsCRUDFile = {
	Create: async (x):number => appendFile(filePath,`{"id:${create_(MapProducts)(x)}","Product":${x}}`),
	Read: async (x): Generic | number => read_(MapMessages)(x),
	Update: async (x, ID): number => update_(MapMessages)(x)(ID),
	Delete: async (x): number => delete_(MapMessages)(x),
	GetAll: async (): Generic => Object.fromEntries(MapMessages),
};

// External Logic
import { PERSISTENCY } from "../process";

const persistency = PERSISTENCY ?? process.env.PERSISTENCY;

export const ProductsCRUD = persistency === "FILE" ? ProductsCRUDFile : ProductsCRUDMemory;
