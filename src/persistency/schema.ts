import { schema } from "normalizr";

const id = new schema.Entity("id");
const nombre = new schema.Entity("nombre");
const apellido = new schema.Entity("apellido");
const edad = new schema.Entity("edad");
const alias = new schema.Entity("alias");
const avatar = new schema.Entity("avatar");
const texto = new schema.Entity("texto");

export const usuario = new schema.Entity("usuario", {
	Id: id,
	Nombre: nombre,
	Apellido: apellido,
	Edad: edad,
	ApellIdo: apellido,
	Alias: alias,
	Avatar: avatar,
});

export const mensaje = new schema.Entity("mensaje", {
	autor: [usuario],
	texto: texto,
});
